grails.servlet.version = "3.0"
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6

//Info : Use for packaging app
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.fork = [

        //Info : Configure settings for the test-app JVM, uses the daemon by default
        test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon: true],

        //Info : Configure settings for the run-app JVM
        run: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve: false],

        //Info : Configure settings for the run-war JVM
        war: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve: false],

        //Info : Configure settings for the Console UI JVM
        console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]

grails.project.dependency.resolver = "maven"
grails.project.dependency.resolution = {
    inherits("global") {
    }

    //Info : Level 'error', 'warn', 'info', 'debug' or 'verbose'
    log "error"
    checksums true
    legacyResolve false

    repositories {
        inherits true

        grailsPlugins()
        grailsHome()
        mavenLocal()
        grailsCentral()
        mavenCentral()

        //Info : Extras repositories
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repo.spring.io/milestone/"
    }

    dependencies {
        //Info : dependencies scopes : 'build', 'compile', 'runtime', 'test' or 'provided'
        // runtime 'mysql:mysql-connector-java:5.1.24'
    }

    plugins {
        //Info : plugins for the build system only
        build ":tomcat:7.0.47"

        //Info :  plugins for the compile step
        compile ':spring-security-core:2.0-RC2'
        compile ":kickstart-with-bootstrap:1.0.0"
        compile ":mail:1.0.1"
        compile ":famfamfam:1.0.1"
        compile ":scaffolding:2.0.1"
        compile ':cache:1.1.1'

        //Info :  plugins needed at runtime but not for compilation
        runtime ":hibernate:3.6.10.6" // or ":hibernate4:4.1.11.6"
        runtime ":database-migration:1.3.8"
        runtime ":jquery:1.10.2.2"
        runtime ":resources:1.2.1"
    }
}
