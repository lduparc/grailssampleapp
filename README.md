#Sample Grails App
————————————

This git repository is a [Grails](http://grails.org) sample project using some  plugins.

For more information refer to Wiki.

#Contributions
————————————

> Most of the part of this project & wiki is related to others grails projects. I just merge some grails plugin into one project and do some modifications on their doucmentation/wiki in order to clarify some points.

You can easily contribute to this project by adding issues. Implement some features or fix. Please use pull-request first.

Thanks to :

* [Grails](http://grails.org)
* [Spring.io](http://spring.io)
* [Kickstart](http://grails.org/plugin/kickstart-with-bootstrap)

#License
————————————

The MIT License (MIT)

Copyright (c) 2014

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
